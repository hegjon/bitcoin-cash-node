`getrpcinfo` JSON-RPC command
=============================

**`getrpcinfo`**

```
Returns details of the RPC server.
```

***

*Bitcoin Cash Node Daemon version v0.21.2*
