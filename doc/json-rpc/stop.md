`stop` JSON-RPC command
=======================

**`stop`**

```
Stop Bitcoin server.
```

***

*Bitcoin Cash Node Daemon version v0.21.2*
